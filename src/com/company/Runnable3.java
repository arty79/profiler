package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Artem Panasyuk on 28.04.2017.
 */
public class Runnable3 implements  Runnable {
    public int i;

    public Runnable3(int i) {
        this.i = i;
    }

    @Override
    public void run() {
        final int j=i;
        System.out.println("Start number: " + j);
        Random random = new Random();
        try {
            Thread.sleep(random.nextInt(10_000));
        } catch (InterruptedException e) {
            System.out.println("Ошибочка");
            e.printStackTrace();
        }
        List<String> stringList = new ArrayList<>();
        for (int k = 0; k < random.nextInt(1_000); k++) {
            stringList.add(k + " Thread of " + j);
        }
        System.out.println("End Thread: " + j);
        saveToFile(stringList.stream().reduce((s1,s2)->s1+s2).toString());
    }

    private void saveToFile(String str){
        try {
            FileWriter fr= new FileWriter("c:\\temp\\dump2.txt");
            fr.write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
