package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sc.next();
        System.out.println("Start");
        Random r = new Random();
        for (int i = 0; i < 1_000; i++) {
            Thread t;
            switch (r.nextInt(5)) {
                case 0:
                    t = new Thread(new Runnable1(i));
                    break;
                case 1:
                    t = new Thread(new Runnable2(i));
                    break;
                case 2:
                    t = new Thread(new Runnable3(i));
                    break;
                case 3:
                    t = new Thread(new Runnable4(i));
                    break;
                default:
                    t = new Thread(new Runnable5(i));
                    break;
            }
            t.setName("Thread" + i);
            t.start();
        }
        sc.next();
        System.out.println("All threads are started");
    }
}